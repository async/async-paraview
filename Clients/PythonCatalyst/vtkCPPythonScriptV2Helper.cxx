// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: BSD-3-Clause
#include "vtkPython.h" // must be first

#include "vtkCPPythonScriptV2Helper.h"

//#include "vtkCollection.h"
#include "vtkObjectFactory.h"
#include "vtkPVLogger.h"
#include "vtkPVTrivialProducer.h"
#include "vtkPythonInterpreter.h"
#include "vtkPythonUtil.h"
//#include "vtkSMExtractTriggerProxy.h"
//#include "vtkSMExtractsController.h"
#include "vtkSMParaViewPipelineController.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSmartPointer.h"
#include "vtkSmartPyObject.h"
#include "vtkStringList.h"

#include <cassert>
#include <map>
#include <string>
#include <vector>

#include "vtkPythonRunLoop.h"

namespace
{
template <typename T>
class vtkScopedSet
{
  T& Ref;
  T Value;

public:
  vtkScopedSet(T& ref, const T& val)
    : Ref(ref)
    , Value(ref)
  {
    this->Ref = val;
  }
  ~vtkScopedSet() { this->Ref = this->Value; }
};
}

class vtkCPPythonScriptV2Helper::vtkInternals
{
public:
  //  vtkSmartPointer<vtkSMExtractsController> ExtractsController;

  // Collection of extractor created in this pipeline.
  // vtkNew<vtkCollection> Extractors;

  // Collection of views created in this pipeline.
  std::vector<vtkSmartPointer<vtkSMProxy>> Views;

  // Collection of trivial producers created for `vtkCPPythonScriptV2Pipeline`.
  std::map<std::string, vtkSmartPointer<vtkSMProxy>> TrivialProducers;

  // Keeps track of arguments
  vtkNew<vtkStringList> ArgumentsList;

  // Keeps track of execute parameters
  vtkNew<vtkStringList> ParametersList;

  // flag that tells us the module has custom "execution" methods.
  bool HasCustomExecutionLogic = false;

  vtkSmartPyObject APIModule;
  vtkSmartPyObject PackageName;
  vtkSmartPyObject Package;

  /**
   * Prepares the user provided Python module/package i.e. makes it available
   * for importing on all ranks.
   *
   * On success, this->PackageName will be set to name that should be used to
   * import the provided package/module.
   */
  bool Prepare(const std::string& fname);

  /**
   * Imports the module.
   */
  bool Import(const std::vector<std::string>& args);

  /**
   * Returns false is any error had occurred and was flushed, otherwise returns
   * true.
   */
  static bool FlushErrors();

  bool AwaitPythonCoroutine(PyObject* functionPtr);

private:
  /**
   * Loads the API module from `paraview` package. Returns false if failed.
   */
  bool LoadAPIModule();

  bool PackageImportFailed = false;
};

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::vtkInternals::LoadAPIModule()
{
  if (this->APIModule)
  {
    return true;
  }

  this->APIModule.TakeReference(PyImport_ImportModule("async_paraview.catalyst.v2_internals"));
  if (!this->APIModule)
  {
    vtkLogF(
      ERROR, "Failed to import required Python module 'async_paraview.catalyst.v2_internals'");
    vtkInternals::FlushErrors();
    return false;
  }
  return true;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::vtkInternals::FlushErrors()
{
  if (PyErr_Occurred())
  {
    PyErr_Print();
    PyErr_Clear();
    return false;
  }
  return true;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::vtkInternals::Prepare(const std::string& fname)
{
#if APV_USE_EXTERNAL_VTK
  vtkPVPythonInterpreterPath();
#endif
  vtkPythonInterpreter::Initialize();
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (!this->LoadAPIModule())
  {
    return false;
  }

  vtkSmartPyObject method(PyUnicode_FromString("register_module"));
  vtkSmartPyObject archive(PyUnicode_FromString(fname.c_str()));
  this->PackageName.TakeReference(
    PyObject_CallMethodObjArgs(this->APIModule, method, archive.GetPointer(), nullptr));
  if (!this->PackageName)
  {
    vtkInternals::FlushErrors();
    return false;
  }

  return true;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::vtkInternals::Import(const std::vector<std::string>& args)
{
  if (!this->PackageName)
  {
    // not "prepared".
    vtkLogF(ERROR, "Cannot determine package name. Did you forget to call 'PrepareFromScript'?");
    return false;
  }

  if (this->Package)
  {
    // already imported.
    return true;
  }

  if (this->PackageImportFailed)
  {
    // avoid re-importing if it already failed.
    return false;
  }

  // populate args.
  this->ArgumentsList->RemoveAllItems();
  for (auto& argcc : args)
  {
    this->ArgumentsList->AddString(argcc.c_str());
  }

  vtkPythonScopeGilEnsurer gilEnsurer;
  vtkSmartPyObject method(PyUnicode_FromString("import_and_validate"));
  this->Package.TakeReference(
    PyObject_CallMethodObjArgs(this->APIModule, method, this->PackageName.GetPointer(), nullptr));
  if (!this->Package)
  {
    vtkInternals::FlushErrors();
    this->PackageImportFailed = true;
    return false;
  }

  vtkSmartPyObject method2(PyUnicode_FromString("has_customized_execution"));
  vtkSmartPyObject result(
    PyObject_CallMethodObjArgs(this->APIModule, method2, this->Package.GetPointer(), nullptr));
  if (!result || !PyBool_Check(result))
  {
    vtkInternals::FlushErrors();
    this->PackageImportFailed = true;
    return false;
  }

  this->HasCustomExecutionLogic = (result == Py_True);
  return true;
}
//----------------------------------------------------------------------------

bool vtkCPPythonScriptV2Helper::vtkInternals::AwaitPythonCoroutine(PyObject* functionPtr)
{
  vtkPythonScopeGilEnsurer gilEnsurer;
  // Get the runing loop
  PyObject* loop = vtkPythonRunLoop::GetInstance()->GetPythonRunLoop();
  if (!loop)
  {
    vtkLogF(ERROR,
      "vtkPythonRunLoop has not an accosiated python event loop.\n"
      "Forgot to call CreateRunLoop/AcquireRunningLoopFromPython ? ");
    return false;
  }
  // TODO we can cache this
  vtkSmartPyObject pyUntil(PyObject_GetAttrString(loop, "run_until_complete"));
  if (!pyUntil)
  {
    vtkInternals::FlushErrors();
    return false;
  }
  // TODO we can cache this
  // create a callable out of the function i.e.
  // callable = functionPtr(args)
  // note that this does not execute the functionPtr. type(callable) returns <class 'coroutine'>
  vtkSmartPyObject pyArgs(PyTuple_New(1));
  PyTuple_SET_ITEM(pyArgs.GetPointer(), 0, this->Package);
  // Set_Item steals a reference , so we need to account for that to avoid
  // reaching 0 ref_count too soon
  Py_INCREF(this->Package);

  vtkSmartPyObject callable(PyObject_CallObject(functionPtr, pyArgs));
  if (!callable)
  {
    vtkInternals::FlushErrors();
    return false;
  }
  // make sure this is a coroutine and not just a regular function
  // having await in a regular function will error anyways but we can make the error more
  // informative here
  if (!PyCoro_CheckExact(callable.GetPointer()))
  {
    vtkLogF(WARNING, "provided method is not a coroutine ! Using await within it will fail");
  }
  // loop.run_until_complete(callable);
  vtkSmartPyObject result(PyObject_CallFunctionObjArgs(pyUntil, callable.GetPointer(), nullptr));

  if (!result)
  {
    vtkInternals::FlushErrors();
    return false;
  }
  return true;
}

//----------------------------------------------------------------------------
vtkCPPythonScriptV2Helper* vtkCPPythonScriptV2Helper::ActiveInstance = nullptr;
//----------------------------------------------------------------------------

vtkStandardNewMacro(vtkCPPythonScriptV2Helper);
vtkCxxSetObjectMacro(vtkCPPythonScriptV2Helper, Options, vtkSMProxy);
//----------------------------------------------------------------------------
vtkCPPythonScriptV2Helper::vtkCPPythonScriptV2Helper()
  : Internals(new vtkCPPythonScriptV2Helper::vtkInternals())
  , Options(nullptr)
{
}

//----------------------------------------------------------------------------
vtkCPPythonScriptV2Helper::~vtkCPPythonScriptV2Helper()
{
  this->SetOptions(nullptr);
  delete this->Internals;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::PrepareFromScript(const std::string& fname)
{
  vtkScopedSet<vtkCPPythonScriptV2Helper*> scoped(vtkCPPythonScriptV2Helper::ActiveInstance, this);

  auto& internals = (*this->Internals);
  if (internals.Prepare(fname))
  {
    this->Filename = fname;
    return true;
  }
  else
  {
    this->Filename = std::string();
    return false;
  }
}

//----------------------------------------------------------------------------
const std::string& vtkCPPythonScriptV2Helper::GetScriptFileName() const
{
  return this->Filename;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::IsImported() const
{
  auto& internals = (*this->Internals);
  return (internals.Package != nullptr);
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::Import(const std::vector<std::string>& args /*={}*/)
{
  vtkScopedSet<vtkCPPythonScriptV2Helper*> scoped(vtkCPPythonScriptV2Helper::ActiveInstance, this);

  auto& internals = (*this->Internals);
  return internals.Import(args);
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::CatalystInitialize()
{
  vtkScopedSet<vtkCPPythonScriptV2Helper*> scoped(vtkCPPythonScriptV2Helper::ActiveInstance, this);

  if (!this->IsImported())
  {
    return false;
  }

  auto& internals = (*this->Internals);
  vtkPythonScopeGilEnsurer gilEnsurer;
  vtkSmartPyObject method = PyObject_GetAttrString(internals.APIModule, "do_catalyst_initialize");
  const bool result = internals.AwaitPythonCoroutine(method.GetPointer());
  if (!result)
  {
    vtkInternals::FlushErrors();
    return false;
  }

  // Set up Extracts controller based on the Catalyst options.
  vtkLogIfF(WARNING, this->Options == nullptr,
    "Catalyst options proxy was not setup in Initialize call. Extracts may not be generated "
    "correctly.");
  // internals.ExtractsController.TakeReference(vtkSMExtractsController::New());
  // if (this->Options)
  //{
  //  internals.ExtractsController->SetExtractsOutputDirectory(
  //    vtkSMPropertyHelper(this->Options, "ExtractsOutputDirectory").GetAsString());
  //}

  return true;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::CatalystFinalize()
{
  if (!this->IsImported())
  {
    return false;
  }

  vtkScopedSet<vtkCPPythonScriptV2Helper*> scoped(vtkCPPythonScriptV2Helper::ActiveInstance, this);

  auto& internals = (*this->Internals);

  vtkPythonScopeGilEnsurer gilEnsurer;
  vtkSmartPyObject method = PyObject_GetAttrString(internals.APIModule, "do_catalyst_finalize");
  const bool result = internals.AwaitPythonCoroutine(method.GetPointer());
  if (!result)
  {
    vtkInternals::FlushErrors();
    return false;
  }

  // if (this->Options &&
  //  vtkSMPropertyHelper(this->Options, "GenerateCinemaSpecification").GetAsInt() == 1)
  //{
  //  vtkVLogF(APV_LOG_CATALYST_VERBOSITY(), "saving Cinema specification");
  //  internals.ExtractsController->SaveSummaryTable(
  //    "data.csv", this->Options->GetSessionProxyManager());
  //}
  internals.Package = nullptr;
  // internals.ExtractsController = nullptr;
  return true;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::CatalystExecute(
  int timestep, double time, const std::vector<std::string>& params)
{
  vtkScopedSet<vtkCPPythonScriptV2Helper*> scoped(vtkCPPythonScriptV2Helper::ActiveInstance, this);

  if (!this->IsImported())
  {
    return false;
  }

  if (!this->IsActivated(timestep, time))
  {
    // skip calling RequestDataDescription.
    return true;
  }

  auto& internals = (*this->Internals);

  // populate execute parameters.
  internals.ParametersList->RemoveAllItems();
  for (auto& param : params)
  {
    internals.ParametersList->AddString(param.c_str());
  }

  // Update ViewTime on each of the views.
  for (auto& view : internals.Views)
  {
    vtkSMPropertyHelper(view, "ViewTime").Set(time);
    view->UpdateVTKObjects();
  }

  vtkPythonScopeGilEnsurer gilEnsurer;

  vtkSmartPyObject method(PyObject_GetAttrString(internals.APIModule, "do_catalyst_execute"));
  const bool result = internals.AwaitPythonCoroutine(method.GetPointer());
  if (!result)
  {
    vtkInternals::FlushErrors();
    return false;
  }

  // Generate extracts from extractor added by this pipeline.
  // internals.ExtractsController->SetTime(time);
  // internals.ExtractsController->SetTimeStep(timestep);
  // internals.ExtractsController->Extract(internals.Extractors);

  // Handle Live.
  // if (this->IsLiveActivated())
  //{
  //  this->DoLive(timestep, time);
  //}
  return true;
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::CatalystResults()
{
  vtkScopedSet<vtkCPPythonScriptV2Helper*> scoped(vtkCPPythonScriptV2Helper::ActiveInstance, this);

  if (!this->IsImported())
  {
    return false;
  }

  auto& internals = (*this->Internals);

  vtkPythonScopeGilEnsurer gilEnsurer;
  vtkSmartPyObject method = PyObject_GetAttrString(internals.APIModule, "do_catalyst_results");
  const bool result = internals.AwaitPythonCoroutine(method.GetPointer());
  if (!result)
  {
    vtkInternals::FlushErrors();
    return false;
  }

  return true;
}

//----------------------------------------------------------------------------
vtkCPPythonScriptV2Helper* vtkCPPythonScriptV2Helper::GetActiveInstance()
{
  return vtkCPPythonScriptV2Helper::ActiveInstance;
}

//----------------------------------------------------------------------------
vtkStringList* vtkCPPythonScriptV2Helper::GetArgumentsAsStringList() const
{
  auto& internals = (*this->Internals);
  return internals.ArgumentsList;
}

vtkStringList* vtkCPPythonScriptV2Helper::GetParametersAsStringList() const
{
  auto& internals = (*this->Internals);
  return internals.ParametersList;
}
//----------------------------------------------------------------------------
void vtkCPPythonScriptV2Helper::RegisterExtractor(vtkSMProxy* extractor)
{
#if 0
  auto& internals = (*this->Internals);
  vtkVLogF(PARAVIEW_LOG_CATALYST_VERBOSITY(), "Registering extractor (%s) for pipeline (%s)",
    vtkLogIdentifier(extractor), vtkLogIdentifier(this));
  internals.Extractors->AddItem(extractor);
#endif
  vtkLogF(ERROR, "Registering extractor is not yet implemented");
}

//----------------------------------------------------------------------------
void vtkCPPythonScriptV2Helper::RegisterView(vtkSMProxy* view)
{
  auto& internals = (*this->Internals);
  vtkVLogF(APV_LOG_CATALYST_VERBOSITY(), "Registering view (%s) for pipeline (%s)",
    vtkLogIdentifier(view), vtkLogIdentifier(this));
  internals.Views.push_back(view);
}

//----------------------------------------------------------------------------
bool vtkCPPythonScriptV2Helper::IsActivated(int timestep, double time)
{
  auto& internals = (*this->Internals);
  /* no live or extractor support yet
  internals.ExtractsController->SetTime(time);
  internals.ExtractsController->SetTimeStep(timestep);

  if (auto globalTrigger = this->Options
      ? vtkSMExtractTriggerProxy::SafeDownCast(
          vtkSMPropertyHelper(this->Options, "GlobalTrigger").GetAsProxy())
      : nullptr)
  {
    if (!globalTrigger->IsActivated(internals.ExtractsController))
    {
      vtkVLogF(APV_LOG_CATALYST_VERBOSITY(), "global trigger not activated for ts=%d, time=%f",
        timestep, time);
      return false;
    }
  }
  */

  if (internals.HasCustomExecutionLogic)
  {
    vtkVLogF(
      APV_LOG_CATALYST_VERBOSITY(), "treating as activated due to presence of custom callbacks");
    return true;
  }

  /* no live or extractor support yet
  if (this->IsLiveActivated())
  {
    vtkVLogF(APV_LOG_CATALYST_VERBOSITY(), "live-trigger activated.");
    return true;
  }

  if (internals.Extractors->GetNumberOfItems() == 0)
  {
    vtkVLogF(APV_LOG_CATALYST_VERBOSITY(),
      "module has no extractors and no custom execution callbacks; is that expected?");
  }

  if (internals.ExtractsController->IsAnyTriggerActivated(internals.Extractors))
  {
    vtkVLogF(APV_LOG_CATALYST_VERBOSITY(), "some extractor activated.");
    return true;
  }
  */

  vtkVLogF(APV_LOG_CATALYST_VERBOSITY(), "nothing activated (ts=%d, time=%f)", timestep, time);
  return false;
}

//----------------------------------------------------------------------------
void vtkCPPythonScriptV2Helper::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
