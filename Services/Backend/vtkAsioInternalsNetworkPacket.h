/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsNetworkPacket.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @class vtkAsioInternalsNetworkPacket
 * @brief vtkAsioInternalsNetworkPacket will be used to pack, send, receive
 * and unpack messages among the vtkAsioServiceEndpoint, vtkAsioService
 * classes.
 *
 * The ServicesAsio module always transmits network packets as little-endian binaries.
 */

#ifndef vtkAsioInternalsNetworkPacket_h
#define vtkAsioInternalsNetworkPacket_h

#include "vtkPacket.h"
#include "vtkServicesBackendModule.h"

#include <memory>
#include <string>

struct VTKSERVICESBACKEND_EXPORT vtkAsioInternalsNetworkPacket
{
  std::string Secret;
  std::string Destination;
  std::string Channel;
  bool ResponseRequired = { false };
  std::shared_ptr<vtkPacket> Payload;

  bool operator==(const vtkAsioInternalsNetworkPacket& other)
  {
    if (this->Payload != nullptr && other.Payload != nullptr)
    {

      return other.Secret == this->Secret && other.Destination == this->Destination &&
        other.Channel == this->Channel && other.ResponseRequired == this->ResponseRequired &&
        *other.Payload == *this->Payload;
    }
    else
    {
      return other.Secret == this->Secret && other.Destination == this->Destination &&
        other.Channel == this->Channel && other.ResponseRequired == this->ResponseRequired &&
        other.Payload == this->Payload;
    }
  }

  bool operator!=(const vtkAsioInternalsNetworkPacket& other) { return !(*this == other); }
};

VTKSERVICESBACKEND_EXPORT std::string ToBytes(const vtkAsioInternalsNetworkPacket& netPacket);
VTKSERVICESBACKEND_EXPORT vtkAsioInternalsNetworkPacket FromBytes(const std::string& bytes);

#endif // vtkAsioInternalsNetworkPacket_h
// VTK-HeaderTest-Exclude: vtkAsioInternalsNetworkPacket.h
