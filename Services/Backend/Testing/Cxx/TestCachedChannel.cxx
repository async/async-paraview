/*=========================================================================

  Program:   Visualization Toolkit
  Module:    TestCachedChannel.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <vtkChannelSubscription.h>
#include <vtkLogger.h>
#include <vtkNew.h>
#include <vtkPacket.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

#include <chrono>
#include <thread>

#define VERIFY(x)                                                                                  \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Failed " #x);                                                                  \
    std::terminate();                                                                              \
  }

namespace
{
bool MakeRequestToPublish(const std::string& channel, int messageId, vtkServiceEndpoint* endpoint)
{
  auto engine = endpoint->GetEngine();
  engine->Await(
    endpoint->SendRequest(vtkPacket({ { "channel", channel }, { "messageId", messageId } })));
  return true;
}
}

int TestCachedChannel(int /*argc*/, char* /*argv*/[])
{
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());

  auto service = engine->CreateService("service1");
  service->GetRequestObservable().subscribe([](const vtkServiceReceiver& receiver) {
    /* every time we receive a request, we publish the same message */
    const auto& packet = receiver.GetPacket();
    const auto channel = packet.GetJSON().at("channel").get<std::string>();
    receiver.Publish(channel, packet);
    receiver.Respond(packet);
  });

  VERIFY(service->Start());

  auto endpoint = engine->CreateServiceEndpoint("service1");
  VERIFY(engine->Await(endpoint->Connect()));

  vtkLogScopeF(INFO, "TestCachedChannel");
  service->EnableChannelCache("channel-cached");

  // Make service publish messages before subscription is made.
  // Ideally, the last one must *not* get lost. However,
  // if the communications thread populates the cached value
  // and the current thread attempts to make a subscription to the
  // observable concurrently, the initial value is lost.
  // On windows, this happens 1 in ~1000 times.
  MakeRequestToPublish("channel-cached", 100, endpoint);
  MakeRequestToPublish("channel-cached", 101, endpoint);
  MakeRequestToPublish("channel-cached", 102, endpoint);

  std::vector<int> receivedIds;
  auto subscription = endpoint->Subscribe("channel-cached");
  subscription->GetObservable().subscribe([&](const vtkPacket& packet) {
    receivedIds.push_back(packet.GetJSON().at("messageId").get<int>());
  });
  endpoint->GetEngine()->Await(subscription->IsReady());

  MakeRequestToPublish("channel-cached", 201, endpoint);
  MakeRequestToPublish("channel-cached", 202, endpoint);
  MakeRequestToPublish("channel-cached", 203, endpoint);

  // Now unsubscribe.
  subscription = nullptr;

  // Now publish a few more messages that should be lost.
  MakeRequestToPublish("channel-cached", 301, endpoint);
  MakeRequestToPublish("channel-cached", 302, endpoint);
  MakeRequestToPublish("channel-cached", 303, endpoint);

  endpoint->GetEngine()->ProcessEventsFor(std::chrono::milliseconds(10));
  for (int x : receivedIds)
  {
    vtkLogF(INFO, "%d", x);
  }
  // Remember that concurrency issue commented earlier? Let's not fail if that happens (rare).
  VERIFY((receivedIds == std::vector<int>({ 102, 201, 202, 203 })) ||
    (receivedIds == std::vector<int>({ 201, 202, 203 })));
  receivedIds.clear();
  service->ClearChannelCache("channel-cached");

  // Now publish a few more messages.
  MakeRequestToPublish("channel-cached", 401, endpoint);
  MakeRequestToPublish("channel-cached", 402, endpoint);
  MakeRequestToPublish("channel-cached", 403, endpoint);

  // subscribe again.
  subscription = endpoint->Subscribe("channel-cached");
  subscription->GetObservable().subscribe([&](const vtkPacket& packet) {
    receivedIds.push_back(packet.GetJSON().at("messageId").get<int>());
  });
  endpoint->GetEngine()->Await(subscription->IsReady());

  service->EnableChannelCache("channel-cached");
  MakeRequestToPublish("channel-cached", 501, endpoint);
  MakeRequestToPublish("channel-cached", 502, endpoint);
  MakeRequestToPublish("channel-cached", 503, endpoint);

  endpoint->GetEngine()->ProcessEventsFor(std::chrono::milliseconds(10));

  // This time, we only expect the messages sent post subscription.
  VERIFY(receivedIds == std::vector<int>({ 501, 502, 503 }));

  // Create a new subscription to the same channel.
  // Since this is cached channel, we'd expect to get the latest packed.
  // this is a different case since here we already have another subscription
  // active for the same channel.
  receivedIds.clear();
  auto subscription2 = endpoint->Subscribe("channel-cached");
  subscription2->GetObservable().subscribe([&](const vtkPacket& packet) {
    receivedIds.push_back(packet.GetJSON().at("messageId").get<int>());
  });
  endpoint->GetEngine()->Await(subscription->IsReady());
  endpoint->GetEngine()->ProcessEventsFor(std::chrono::milliseconds(10));

  VERIFY(receivedIds == std::vector<int>({ 503 }));

  engine->Finalize();
  return EXIT_SUCCESS;
}
