/*=========================================================================

  Program:   ParaView
  Module:    TestSendRequest.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <chrono>

#include <vtkNew.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

#define CHECK_ERROR(header, status)                                                                \
  if (!status)                                                                                     \
  {                                                                                                \
    vtkLog(ERROR, << header << ". Error occurred.");                                               \
    engine->Finalize();                                                                            \
    return 1;                                                                                      \
  }

int TestSendRequest(int /*argc*/, char* /*argv*/[])
{
  bool success = false;
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());
  auto ds = engine->CreateService("services.data");
  auto dsSubscription =
    ds->GetRequestObservable(true)
      .filter([](const vtkServiceReceiver& receiver) {
        try
        {
          return receiver.GetPacket().GetJSON().at("type").get<std::string>() == "echo";
        }
        catch (std::out_of_range&)
        {
          return true;
        }
      })
      .observe_on(ds->GetRunLoopScheduler())
      .subscribe([ds](const vtkServiceReceiver& receiver) {
        vtkLogScopeF(INFO, "echoing: %s", receiver.GetPacket().ToString().c_str());
        receiver.Respond(receiver.GetPacket());
      });
  success = ds->Start();
  auto rs = engine->CreateService("services.render");
  auto rsSubscription =
    rs->GetRequestObservable(true)
      .filter([](const vtkServiceReceiver& receiver) {
        try
        {
          return receiver.GetPacket().GetJSON().at("type").get<std::string>() == "echo";
        }
        catch (std::out_of_range&)
        {
          return true;
        }
      })
      .observe_on(rs->GetRunLoopScheduler())
      .subscribe([rs](const vtkServiceReceiver& receiver) {
        vtkLogScopeF(INFO, "echoing: %s", receiver.GetPacket().ToString().c_str());
        receiver.Respond(receiver.GetPacket());
      });
  success &= rs->Start();
  auto dsep = engine->CreateServiceEndpoint("services.data");
  auto rsep = engine->CreateServiceEndpoint("services.render");
  CHECK_ERROR("Service startup", success)

  auto o1 = dsep->Connect();
  auto o2 = rsep->Connect();
  auto connected = o1.combine_latest(o2)
                     .observe_on(engine->GetCoordination())
                     .map([](const std::tuple<bool, bool>& status) {
                       return std::get<0>(status) && std::get<1>(status);
                     });
  success &= engine->Await(connected.as_dynamic());
  CHECK_ERROR("Connect endpoints with services", success)

  auto reply1 = dsep->SendRequest(vtkNJson({ { "type", "echo" }, { "tag", 1 } }));
  auto reply2 = rsep->SendRequest(vtkNJson({ { "type", "echo" }, { "tag", 2 } }));
  auto reply3 = dsep->SendRequest(vtkNJson({ { "type", "echo" }, { "tag", 3 } }));
  auto reply4 = rsep->SendRequest(vtkNJson({ { "type", "echo" }, { "tag", 4 } }));
  success &= engine->Await(reply1).GetJSON().at("tag") == 1;
  success &= engine->Await(reply2).GetJSON().at("tag") == 2;
  success &= engine->Await(reply3).GetJSON().at("tag") == 3;
  success &= engine->Await(reply4).GetJSON().at("tag") == 4;
  dsSubscription.unsubscribe();
  rsSubscription.unsubscribe();
  CHECK_ERROR("Two-way simple communication between 2 endpoints and 2 services", success)

  engine->Finalize();
  return 0;
}
