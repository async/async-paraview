set(classes
  vtkPacket
  vtkServicesEngine
  vtkService
  vtkServiceEndpoint
  vtkRemotingCoreUtilities
  vtkProvider
  vtkChannelSubscription
  vtkServicesCoreLogVerbosity
)

set(nowrap_classes
  vtkReactiveCommand)

set(headers
  vtkNJson.h
  vtkNJsonFwd.h)

set(templates
  vtkServicesEngine.txx
  vtkRemotingCoreUtilities.txx)

vtk_module_add_module(AsyncParaView::ServicesCore
  CLASSES         ${classes}
  NOWRAP_CLASSES  ${nowrap_classes}
  HEADERS         ${headers}
  TEMPLATES       ${templates})

vtk_module_remoting_exclude()
