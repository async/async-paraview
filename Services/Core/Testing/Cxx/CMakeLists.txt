vtk_add_test_cxx(vtkServicesCoreCxxTests tests
  NO_VALID
  TestPacket.cxx
  TestRxCPPBehavior.cxx)

vtk_test_cxx_executable(vtkServicesCoreCxxTests tests)
