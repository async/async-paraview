/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkServiceEndpoint.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkServiceEndpoint.h"

#include "vtkChannelSubscription.h"
#include "vtkLogger.h"
#include "vtkNJson.h"
#include "vtkObjectFactory.h"
#include "vtkPacket.h"
#include "vtkRemotingCoreUtilities.h"
#include "vtkServicesCoreLogVerbosity.h"
#include "vtkServicesEngine.h"
#include "vtkSmartPointer.h"

namespace
{
vtkPacket GetEmptyPacket()
{
  return vtkPacket(vtkNJson{ { "__is_initial_value__", true } });
}
}

class vtkServiceEndpoint::vtkInternals
{
public:
  const std::thread::id OwnerTID{ std::this_thread::get_id() };
  vtkServicesEngine* Engine{ nullptr };
  std::atomic<bool> Connected{ false };
  std::atomic<bool> Shutdown{ false };
  std::string ServiceName;
  std::string Url;

  rxcpp::subjects::subject<std::tuple<std::string, vtkPacket>> ChannelSubject;

  std::mutex SubscriptionsMutex;
  struct SubscriptionInfo
  {
    int32_t ReferenceCount{ 0 };

    rxcpp::subjects::subject<vtkPacket> Subject;
    rxcpp::subjects::behavior<vtkPacket> Behavior{ ::GetEmptyPacket() };
    rxcpp::observable<vtkPacket> GetObservable() const
    {
      auto o1 = this->Subject.get_observable();
      auto o2 = this->Behavior.get_observable().filter([](const vtkPacket& packet) {
        const auto& data = packet.GetJSON();
        return data.find("__is_initial_value__") == data.end();
      });
      return o1.merge(o2);
    }

    void operator=(const SubscriptionInfo&) = delete;
    SubscriptionInfo(const SubscriptionInfo&) = delete;
    SubscriptionInfo() = default;
    ~SubscriptionInfo() { this->Subject.get_subscriber().on_completed(); }
  };

  std::map<std::string, SubscriptionInfo> Subscriptions;
};

vtkAbstractObjectFactoryNewMacro(vtkServiceEndpoint);
//----------------------------------------------------------------------------
vtkServiceEndpoint::vtkServiceEndpoint()
  : Internals(new vtkServiceEndpoint::vtkInternals())
{
}

//----------------------------------------------------------------------------
vtkServiceEndpoint::~vtkServiceEndpoint()
{
  const auto& internals = (*this->Internals);
  vtkLogIfF(ERROR, internals.Connected || !internals.Shutdown, "Did you forget to call shutdown?");
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
}

//----------------------------------------------------------------------------
void vtkServiceEndpoint::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
void vtkServiceEndpoint::Initialize(
  vtkServicesEngine* engine, const std::string& serviceName, const std::string& sessionUrl)
{
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  internals.Engine = engine;
  internals.ServiceName = serviceName;
  internals.Url = sessionUrl;
  try
  {
    this->InitializeInternal();
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Failed to initialize");
    std::terminate();
  }
}

//----------------------------------------------------------------------------
void vtkServiceEndpoint::UnsetEngine()
{
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  internals.Engine = nullptr;
}

//----------------------------------------------------------------------------
vtkServicesEngine* vtkServiceEndpoint::GetEngine() const
{
  auto& internals = (*this->Internals);
  return internals.Engine;
}

//----------------------------------------------------------------------------
rxcpp::observable<bool> vtkServiceEndpoint::Connect()
{
  auto& internals = (*this->Internals);
  if (internals.Connected)
  {
    vtkVLogF(VTKSERVICESCORE_LOG_VERBOSITY(), "already connected to a service!");
    return rxcpp::observable<>::just(true);
  }
  if (internals.Shutdown)
  {
    vtkLogF(ERROR, "Cannot reused a stale endpoint!");
    return rxcpp::observable<>::just(false);
  }

  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  internals.Connected = true;

  try
  {
    return this->ConnectInternal(internals.ServiceName, internals.Url)
      .observe_on(this->GetEngine()->GetCoordination());
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Failed to Connect :\n%s", exception.what());
    return rxcpp::observable<>::just(false);
  }
}

//----------------------------------------------------------------------------
void vtkServiceEndpoint::Shutdown()
{
  vtkVLogScopeF(VTKSERVICESCORE_LOG_VERBOSITY(), "%s: Shutdown", vtkLogIdentifier(this));
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  if (!internals.Connected || internals.Shutdown)
  {
    return;
  }
  internals.Connected = false;
  internals.Shutdown = true;

  {
    // release all subscriptions
    std::lock_guard<std::mutex> lock(internals.SubscriptionsMutex);
    internals.Subscriptions.clear();
  }

  try
  {
    this->ShutdownInternal();
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Failed to Shutdown :\n%s", exception.what());
    std::terminate();
  }
}

//----------------------------------------------------------------------------
const std::string& vtkServiceEndpoint::GetServiceName() const
{
  const auto& internals = (*this->Internals);
  return internals.ServiceName;
}

//----------------------------------------------------------------------------
const std::string& vtkServiceEndpoint::GetServiceUrl() const
{
  const auto& internals = (*this->Internals);
  return internals.Url;
}

//----------------------------------------------------------------------------
void vtkServiceEndpoint::SendAMessage(const vtkPacket& packet) const
{
  const auto& internals = (*this->Internals);
  if (internals.Shutdown)
  {
    return;
  }
  if (!internals.Connected)
  {
    vtkLogF(ERROR, "not connected yet!");
    return;
  }

  try
  {
    this->SendMessageInternal(packet);
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Failed to send message :\n%s", exception.what());
    std::terminate();
  }
}

//----------------------------------------------------------------------------
rxcpp::observable<vtkPacket> vtkServiceEndpoint::SendRequest(const vtkPacket& packet) const
{
  auto& internals = (*this->Internals);
  if (internals.Shutdown)
  {
    return rxcpp::observable<>::just(vtkPacket());
  }

  if (!internals.Connected)
  {
    vtkLogF(ERROR, "not connected yet!");
    return rxcpp::observable<>::just(vtkPacket());
  }

  try
  {
    return this
      ->SendRequestInternal(packet)
      // This is important ! The request is completed by the RPC/io_context
      // thread but we want the completion handler to be executed by the main
      // thread. This is preferred for a couple of reasons.
      //
      // - consumers of this API do not need to guarantee that
      // the callback code is threadsafe.
      //
      // - In python, vtkPythonRunLoop connects the main thread runloop ( ==
      // this->Session->GetCoordination() in this case)  with asyncio internal
      // loop so, in order for an event to be "seen" from python it needs to be
      // emitted on the main thread's coordination.
      .observe_on(this->GetEngine()->GetCoordination());
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Failed to send request :\n%s", exception.what());
    std::terminate();
  }
}

//----------------------------------------------------------------------------
vtkSmartPointer<vtkChannelSubscription> vtkServiceEndpoint::Subscribe(const std::string& channel)
{
  auto& internals = (*this->Internals);
  return this->Subscribe(channel, this->GetEngine()->GetCoordination());
}

//----------------------------------------------------------------------------
vtkSmartPointer<vtkChannelSubscription> vtkServiceEndpoint::Subscribe(
  const std::string& channel, const rxcpp::observe_on_one_worker& coordination)
{
  auto& internals = (*this->Internals);
  if (internals.Shutdown)
  {
    return nullptr;
  }

  std::lock_guard<std::mutex> lock(internals.SubscriptionsMutex);

  auto subscription = vtk::TakeSmartPointer(vtkChannelSubscription::New());

  auto& info = internals.Subscriptions[channel];
  info.ReferenceCount += 1;
  auto observable = info.GetObservable().observe_on(coordination);
  if (info.ReferenceCount == 1)
  {
    try
    {
      auto response = this->SubscribeInternal(channel);
      subscription->Initialize(this, channel, std::move(observable), response);
    }
    catch (const std::exception& exception)
    {
      vtkLogF(ERROR, "Failed to Subscribe\n%s", exception.what());
      return nullptr;
    }
  }
  else
  {
    subscription->Initialize(this, channel, std::move(observable));
  }

  /*
      internals.ChannelSubject.get_observable()
        .filter([channel](const std::tuple<std::string, vtkPacket>& tuple) {
          return std::get<0>(tuple) == channel;
        })
        .map([](const std::tuple<std::string, vtkPacket>& tuple) { return std::get<1>(tuple); })
        .observe_on(coodination);
        */
  return subscription;
}

//----------------------------------------------------------------------------
void vtkServiceEndpoint::Unsubscribe(const std::string& channel)
{
  auto& internals = (*this->Internals);
  if (internals.Shutdown)
  {
    return;
  }
  if (!internals.Connected)
  {
    vtkLogF(ERROR, "not connected yet!");
    return;
  }

  std::lock_guard<std::mutex> lock(internals.SubscriptionsMutex);
  auto iter = internals.Subscriptions.find(channel);
  if (iter == internals.Subscriptions.end())
  {
    vtkLogF(ERROR, "no such channel to unsubscribe: %s", channel.c_str());
    return;
  }
  const auto count = --iter->second.ReferenceCount;
  if (count == 0)
  {
    internals.Subscriptions.erase(iter);
    try
    {
      this->UnsubscribeInternal(channel);
    }
    catch (const std::exception& exception)
    {
      vtkLogF(ERROR, "Failed to Unsubscribe\n%s", exception.what());
      return;
    }
  }
}

//----------------------------------------------------------------------------
void vtkServiceEndpoint::DispatchOnChannel(
  const std::string& channel, const vtkPacket& packet, bool cached) const
{
  auto& internals = (*this->Internals);
  std::unique_lock<std::mutex> lock(internals.SubscriptionsMutex);
  auto iter = internals.Subscriptions.find(channel);
  if (iter != internals.Subscriptions.end())
  {
    auto cachedSubscriber = iter->second.Behavior.get_subscriber();
    auto subscriber = iter->second.Subject.get_subscriber();
    lock.unlock();

    if (cached)
    {
      cachedSubscriber.on_next(packet);
    }
    else
    {
      cachedSubscriber.on_next(::GetEmptyPacket());
      subscriber.on_next(packet);
    }
  }
}
