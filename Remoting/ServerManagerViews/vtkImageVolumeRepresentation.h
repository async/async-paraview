/*=========================================================================

  Program:   ParaView
  Module:    vtkImageVolumeRepresentation.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkImageVolumeRepresentation
 * @brief   representation for showing image
 * datasets as a volume.
 *
 * vtkImageVolumeRepresentation is a representation for volume rendering
 * vtkImageData. Unlike other data-representations used by ParaView, this
 * representation does not support delivery to client (or render server) nodes.
 * In those configurations, it merely delivers a outline for the image to the
 * client and render-server and those nodes simply render the outline.
 *
 * vtkImageVolumeRepresentation has a few caveats:
 *
 * 1. It only supports vtkImageData and vtkPartitionedDataSet
 *    comprising of vtkImageData. In case of latter, any partition not a
 *    vtkImageData will be silently skipped.
 *
 * 2. In distributed mode, bounds on each rank as assumed to be non-overlapping.
 */

#ifndef vtkImageVolumeRepresentation_h
#define vtkImageVolumeRepresentation_h

#include "vtkRemotingServerManagerViewsModule.h" //needed for exports

#include "vtkVolumeRepresentation.h"

#include <memory> // for unique_ptr
#include <vector> // for std::vector

class vtkColorTransferFunction;
class vtkDataSet;
class vtkExtentTranslator;
class vtkFixedPointVolumeRayCastMapper;
class vtkImageData;
class vtkImplicitFunction;
class vtkOutlineSource;
class vtkPiecewiseFunction;
class vtkPolyDataMapper;
class vtkPVLODVolume;
class vtkVolumeMapper;
class vtkVolumeProperty;

class VTKREMOTINGSERVERMANAGERVIEWS_EXPORT vtkImageVolumeRepresentation
  : public vtkVolumeRepresentation
{
public:
  static vtkImageVolumeRepresentation* New();
  vtkTypeMacro(vtkImageVolumeRepresentation, vtkVolumeRepresentation);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Method to set isosurface values.
   */
  void SetIsosurfaceValues(const std::vector<double>& values);
  //***************************************************************************
  // Forwarded to vtkSmartVolumeMapper/vtkMultiBlockVolumeMapper.
  void SetRequestedRenderMode(int);
  void SetBlendMode(int);
  void SetCropping(int);

  //@{
  /**
   * Set the cropping origin.
   */
  void SetCroppingOrigin(double value0, double value1, double value2);
  //@}

  //@{
  /**
   * Set the cropping scale.
   */
  void SetCroppingScale(double value0, double value1, double value2);
  //@}

protected:
  vtkImageVolumeRepresentation();
  ~vtkImageVolumeRepresentation() override;

  /**
   * Fill input port information.
   */
  int FillInputPortInformation(int port, vtkInformation* info) override;

  /**
   * Passes on parameters to the active volume mapper
   */
  virtual void UpdateMapperParameters();

  // vtkPVDataRepresentation API
  ///@{
  void InitializeForDataProcessing() override;
  void InitializeForRendering() override;
  bool AddToView(vtkPVView* view) override;
  bool RemoveFromView(vtkPVView* view) override;
  bool RequestDataToRender(
    vtkInformationVector** inputVector, vtkInformationVector* outputVector) override;
  bool RequestPrepareForRender(vtkInformation* request, vtkInformationVector* inputVector) override;
  bool RequestRender(vtkInformation* request) override;
  ///@}
private:
  vtkImageVolumeRepresentation(const vtkImageVolumeRepresentation&) = delete;
  void operator=(const vtkImageVolumeRepresentation&) = delete;

  class vtkRenderingInternals;

  std::unique_ptr<vtkRenderingInternals> RInternals;
};

#endif
