/*=========================================================================

  Program:   ParaView
  Module:    vtkClientSessionViews.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkClientSessionViews.h"

#include "vtkChannelSubscription.h"
#include "vtkObjectFactory.h"
#include "vtkPacket.h"
#include "vtkRemoteObjectProviderViews.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMViewProxy.h"
#include "vtkServiceEndpoint.h"
#include "vtkSmartPointer.h"

class vtkClientSessionViews::vtkInternals
{
public:
  std::vector<vtkSmartPointer<vtkChannelSubscription>> Subscriptions;
  std::vector<rxcpp::composite_subscription> RxSubscriptions;
};

vtkObjectFactoryNewMacro(vtkClientSessionViews);
//----------------------------------------------------------------------------
vtkClientSessionViews::vtkClientSessionViews()
  : Internals(new vtkClientSessionViews::vtkInternals())
{
}

//----------------------------------------------------------------------------
vtkClientSessionViews::~vtkClientSessionViews() = default;

//----------------------------------------------------------------------------
void vtkClientSessionViews::InitializeServiceEndpoint(vtkServiceEndpoint* endpoint)
{
  this->Superclass::InitializeServiceEndpoint(endpoint);
  auto& internals = (*this->Internals);

  if (endpoint->GetServiceName() == "rs")
  {
    auto* pxm = this->GetProxyManager();
    auto cSubscription = endpoint->Subscribe(vtkRemoteObjectProviderViews::GetImageChannelName());
    internals.RxSubscriptions.emplace_back(
      cSubscription->GetObservable().subscribe([pxm](const vtkPacket& packet) {
        const auto& json = packet.GetJSON();
        const auto gid = json.at("GlobalID").get<vtkTypeUInt32>();
        auto proxy = pxm->FindProxy(gid);
        if (auto* view = vtkSMViewProxy::SafeDownCast(proxy))
        {
          view->PostRenderingResult(packet);
          view->RequestProgressiveRenderingPassIfNeeded();
        }
      }));

    internals.Subscriptions.push_back(cSubscription);
  }
}

//----------------------------------------------------------------------------
void vtkClientSessionViews::FinalizeServiceEndpoint(vtkServiceEndpoint* endpoint)
{
  this->Superclass::FinalizeServiceEndpoint(endpoint);

  auto& internals = (*this->Internals);
  for (auto& subscription : internals.RxSubscriptions)
  {
    subscription.unsubscribe();
  }
  internals.RxSubscriptions.clear();
  internals.Subscriptions.clear();
}

//----------------------------------------------------------------------------
void vtkClientSessionViews::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
