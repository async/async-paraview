/*=========================================================================

  Program:   ParaView
  Module:    vtkPVPythonApplication.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkPVPythonApplication
 * @brief A subclass of vtkPVApplication with a wrapper-friendlier API
 *
 */

#ifndef vtkPVPythonApplication_h
#define vtkPVPythonApplication_h

#include "vtkRemotingServerManagerPythonModule.h" //needed for exports

#include "vtkPVApplication.h"
#include "vtkPythonObservableWrapper.h" // for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE
#include "vtkPythonRunLoop.h"
#include "vtkSmartPointer.h" // for vtkSmartPointer

#include <memory> // for std::unique_ptr
#include <string> // for std::string

class vtkPythonRunLoop;

class VTKREMOTINGSERVERMANAGERPYTHON_EXPORT vtkPVPythonApplication : public vtkPVApplication
{
public:
  static vtkPVPythonApplication* New();
  vtkTypeMacro(vtkPVPythonApplication, vtkPVApplication);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Provides access to the singleton once it has been created and initialized.
   */
  static vtkPVPythonApplication* GetInstance();

  bool InitializeUsingPython(
    const std::string& executable, vtkMultiProcessController* globalController = nullptr);

  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(vtkTypeUInt32, CreateBuiltinSession());
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(vtkTypeUInt32, CreateRemoteSession(const std::string&));

protected:
  vtkPVPythonApplication();
  ~vtkPVPythonApplication() override;

  void FinalizeInternal() override;

private:
  vtkPVPythonApplication(const vtkPVPythonApplication&) = delete;
  void operator=(const vtkPVPythonApplication&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
