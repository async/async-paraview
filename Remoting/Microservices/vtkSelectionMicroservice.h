/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkSelectionMicroservice.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkSelectionMicroservice
 * @brief A microservice that allows to perform selections.
 *
 * @note Only query selections are enabled for now.
 * @note For now we just wwrap pipelinebuilder calls but once we have
 * interactive selections this microservice will handle all the complexity for
 * DS<->RS communication.
 */

#ifndef vtkSelectionMicroservice_h
#define vtkSelectionMicroservice_h

#include "vtkObject.h"

#include "vtkDataObject.h"                  // for vtkDataObject::AttributeTypes
#include "vtkPythonObservableWrapper.h"     // for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE
#include "vtkRemotingMicroservicesModule.h" // for exports
#include "vtkSMSourceProxy.h"               // for return

#include <memory> // for std::unique_ptr
#include <string> // for arg
#include <vector> // for arg

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkClientSession;

class VTKREMOTINGMICROSERVICES_EXPORT vtkSelectionMicroservice : public vtkObject
{
public:
  static vtkSelectionMicroservice* New();
  vtkTypeMacro(vtkSelectionMicroservice, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  enum SelectionFieldType
  {
    // points and cells for now
    POINT = vtkDataObject::POINT,
    CELL = vtkDataObject::CELL,
    NUMBER_OF_ATTRIBUTE_TYPES
  };

  /**
   * Create a selecton based on a query.
   *
   * @param query: numpy-like query for selection
   * @param elementType: type of elements to selection should be a SelectionFieldType value.
   * @param invert: wehther to invert the selection criterion.
   *
   * @note requires Python enabled
   */
  rxcpp::observable<vtkSMSourceProxy*> QuerySelect(
    const std::string& query, int elementType, bool invert);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    vtkObject*, QuerySelect(const std::string& query, int elementType, bool invert));

  /**
   * Apply Selection on producer and return a source that holds the extracted elements.
   */
  rxcpp::observable<vtkSMSourceProxy*> Extract(
    vtkSMSourceProxy* producer, vtkSMSourceProxy* selection);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    vtkObject*, Extract(vtkSMSourceProxy* producer, vtkSMSourceProxy* selection));

  ///@{
  /**
   * Get/set the session. Changing the session will cause the current/selection to
   * change and hence trigger `on_next` call on the subscribed observables, if any.
   */
  void SetSession(vtkClientSession* session);
  vtkClientSession* GetSession() const;
  ///@}

protected:
  vtkSelectionMicroservice();
  ~vtkSelectionMicroservice() override;

private:
  vtkSelectionMicroservice(const vtkSelectionMicroservice&) = delete;
  void operator=(const vtkSelectionMicroservice&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
