/*=========================================================================

  Program:   ParaView
  Module:    vtkRemoteFileSystemProvider.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkRemoteFileSystemProvider.h"

#include "vtkCollection.h"
#include "vtkCollectionIterator.h"
#include "vtkDirectory.h"
#include "vtkMultiProcessController.h"
#include "vtkMultiProcessStream.h"
#include "vtkNJson.h"
#include "vtkPVDataInformation.h"
#include "vtkPVFileInformation.h"
#include "vtkPVLogger.h"
#include "vtkPacket.h"
#include "vtkService.h"
#include "vtkServicesCoreLogVerbosity.h"
#include "vtkSmartPointer.h"
#include "vtksys/Status.hxx"
#include "vtksys/SystemTools.hxx"

#include <exception>

bool StartsWith(const std::string& path, const std::string& token)
{
  // rfind(token,position) find the last occurance of token in path starting at
  // position 0 and proceeding from right to left could be replaced with
  // starts_with in C++20
  return path.rfind(token, 0) == 0;
}

class vtkRemoteFileSystemProvider::vtkInternals
{
public:
  rxcpp::composite_subscription Subscription;
  vtkRemoteFileSystemProvider* Self = nullptr;

  vtkInternals(vtkRemoteFileSystemProvider* self)
    : Self(self)
  {
  }

  ~vtkInternals() { this->Subscription.unsubscribe(); }

  void Preview(const vtkPacket& packet){};

  vtkPacket Process(const vtkPacket& packet);

  std::map<std::string, std::string> Mapped2RealPath;

private:
  vtkNJson MakeDirectory(const vtkNJson& request) const;
  vtkNJson Remove(const vtkNJson& request) const;
  vtkNJson Rename(const vtkNJson& request) const;
  vtkNJson ListDirectory(const vtkNJson& request) const;
  vtkNJson FileExists(const vtkNJson& request) const;
  vtkNJson AddRootDirectory(const vtkNJson& request);
  vtkNJson RemoveRootDirectory(const vtkNJson& request);
  vtkNJson ConvertMappedPaths(const vtkNJson& request) const;

  // utilities
  bool IsPathAllowed(const std::string& mappedPath, std::string& realPath) const;
  void TransformPaths(vtkSmartPointer<vtkPVFileInformation> info) const;
  bool MapPath(std::string& realPath) const;
  bool IsMappedRoot(const std::string& path, vtkSmartPointer<vtkPVFileInformation> info) const;

  std::string PermissionErrorMessage(const std::string& path) const
  {
    return "Path is not allowed\n" + path;
  }

  // call one of the requests while handling the symnmetric MPI case: even though
  // `run_on_root_only` commands only execute on the root all of the processes
  // should get a status/information.
  template <typename F>
  vtkNJson Call(const vtkNJson& request, F&& operation)
  {
    // this is a vtkLocalFileSysttemProvider and thus no service is associtated with it
    if (!this->Self->GetService())
    {
      return (this->*operation)(request);
    }
    const bool isSymmetric = request.at("client_uses_symmetric_mpi").get<bool>();
    auto controller = this->Self->GetController();
    vtkNJson reply;
    if (!isSymmetric || controller->GetLocalProcessId() == 0)
    {
      reply = (this->*operation)(request);
    }

    if (isSymmetric)
    {
      vtkMultiProcessStream stream;
      if (controller->GetLocalProcessId() == 0)
      {
        auto buffer = vtkNJson::to_cbor(reply);
        stream << static_cast<unsigned int>(buffer.size());
        stream.Push(reinterpret_cast<unsigned char*>(buffer.data()),
          static_cast<unsigned int>(buffer.size()));
      }
      controller->Broadcast(stream, 0);
      if (controller->GetLocalProcessId() > 0)
      {
        unsigned int size = 0;
        stream >> size;
        std::vector<uint8_t> buffer(size);
        unsigned char* ptr = reinterpret_cast<unsigned char*>(buffer.data());
        stream.Pop(ptr, size);
        reply = vtkNJson::from_cbor(buffer);
      }
    }
    return reply;
  }
};
//-----------------------------------------------------------------------------
// This is a special case handling. When we are using mapped paths. "/" is
// normally not allowed. Instead of just erroring out we return a "virtual"
// path that contains all the mapped locations as children of "/".
bool vtkRemoteFileSystemProvider::vtkInternals::IsMappedRoot(
  const std::string& path, vtkSmartPointer<vtkPVFileInformation> info) const
{
  if (this->Mapped2RealPath.empty())
  {
    return false;
  }
#if defined(_WIN32) && !defined(__CYGWIN__)
  const std::string rootPath = "\\";
#else
  const std::string rootPath = "/";
#endif
  if (path != rootPath)
  {
    return false;
  }
  // we do not have access to all the fields of vtkPVFileInformation so instead
  // we create a state what has all the required infor and initialize `info`
  std::time_t now = std::time(nullptr);
  vtkNJson templateState;
  // see vtkPVFileInformation::Save/LoadInformation
  templateState["root_only"] = true;
  templateState["name"] = rootPath;
  templateState["full_path"] = rootPath;
  templateState["type"] = vtkPVFileInformation::DIRECTORY;
  templateState["hidden"] = false;
  templateState["number_of_items"] = this->Mapped2RealPath.size();
  templateState["extension"] = "";
  templateState["size"] = 0;
  templateState["modification_time"] = size_t(now); // just make up some time so it is not empty
  templateState["path_separator"] = info->GetPathSeparator();
  templateState["children"] = vtkNJson::array();
  auto children = vtkNJson::array();

  vtkNJson rootState = templateState;
  for (const auto& item : this->Mapped2RealPath)
  {
    vtkNJson childState = templateState;
    childState["full_path"] = item.first;
    childState["name"] = item.first.substr(1); // drop "/"
    childState["number_of_items"] = 0;
    children.push_back(childState);
  }

  rootState["children"] = children;

  info->Initialize();
  info->LoadInformation(rootState);

  return true;
}

//-----------------------------------------------------------------------------
// Check whether the provided mappedPath is a sub-path of the allowed
// rootpaths. If yes, return true and convert it to an absolute real path.
// Otherwise return false and expand the mappedPath to an absolute path.
bool vtkRemoteFileSystemProvider::vtkInternals::IsPathAllowed(
  const std::string& mappedPath, std::string& path) const
{
  path =
    vtksys::SystemTools::ConvertToOutputPath(vtksys::SystemTools::CollapseFullPath(mappedPath));
  if (this->Mapped2RealPath.empty())
  {
    return true;
  }
  else
  {
    for (const auto& item : this->Mapped2RealPath)
    {
      const std::string& itemMappedPath = item.first;
      const std::string& itemRealPath = item.second;
      if (StartsWith(path, itemMappedPath))
      {
        path.replace(0, itemMappedPath.size(), itemRealPath);
        return true;
      }
    }
  }
  return false;
}
//-----------------------------------------------------------------------------
// Map a realPath on location to a MappedPath. Essentially the inverse of IsPathAllowed
bool vtkRemoteFileSystemProvider::vtkInternals::MapPath(std::string& path) const
{
  if (this->Mapped2RealPath.empty())
  {
    return true;
  }
  else
  {
    for (const auto& item : this->Mapped2RealPath)
    {
      const std::string itemMappedPath = item.first;
      const std::string itemRealPath = item.second;
      if (StartsWith(path, itemRealPath))
      {
        path.replace(0, itemRealPath.size(), itemMappedPath);
        return true;
      }
    }
  }
  return false;
}
//-----------------------------------------------------------------------------
// Transform all paths in `info` from realpaths to mappedpaths
void vtkRemoteFileSystemProvider::vtkInternals::TransformPaths(
  vtkSmartPointer<vtkPVFileInformation> info) const
{
  if (this->Mapped2RealPath.empty())
  {
    return;
  }
  std::string newPath = info->GetFullPath();

  if (!MapPath(newPath))
  {
    vtkLogF(ERROR, "Could not map path back to real!");
    vtkLogF(ERROR, "%s", newPath.c_str());
    info->Initialize();
    return;
  }

  info->SetFullPath(newPath.c_str());
  vtkSmartPointer<vtkCollectionIterator> iter;
  iter.TakeReference(info->Contents->NewIterator());
  for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
  {
    vtkPVFileInformation* child = vtkPVFileInformation::SafeDownCast(iter->GetCurrentObject());
    TransformPaths(child);
  }
}

//----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::MakeDirectory(const vtkNJson& request) const
{
  std::string path = request.at("path").get<std::string>();
  const bool isSymmetric = request.at("client_uses_symmetric_mpi").get<bool>();
  vtkNJson reply;
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "MakeDirectory(%s)", path.c_str());
  std::string fullpath;
  if (!this->IsPathAllowed(path, fullpath))
  {
    reply["status"] = false;
    reply["__vtk_error_message__"] = PermissionErrorMessage(path);
    return reply;
  }
  vtksys::Status status = vtksys::SystemTools::MakeDirectory(fullpath);
  reply["status"] = status.IsSuccess();
  if (!status.IsSuccess())
  {
    reply["__vtk_error_message__"] = status.GetString();
  }
  return reply;
}

//----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::Remove(const vtkNJson& request) const
{
  const std::string path = request.at("path").get<std::string>();
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "Remove(%s)", path.c_str());
  vtkNJson reply;
  std::string fullpath;
  if (!this->IsPathAllowed(path, fullpath))
  {
    reply["status"] = false;
    reply["__vtk_error_message__"] = PermissionErrorMessage(path);
    return reply;
  }
  vtksys::Status status;
  if (vtksys::SystemTools::PathExists(fullpath))
  {
    if (vtksys::SystemTools::FileIsDirectory(fullpath))
    {
      status = vtksys::SystemTools::RemoveADirectory(fullpath);
    }
    else
    {
      status = vtksys::SystemTools::RemoveFile(fullpath);
    }

    reply["status"] = status.IsSuccess();
    if (!status.IsSuccess())
    {
      reply["__vtk_error_message__"] = status.GetString();
    }
  }
  else
  {
    reply["status"] = false;
    reply["__vtk_error_message__"] = "Path does not exist";
  }
  return reply;
}

//----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::Rename(const vtkNJson& request) const
{
  const std::string& oldPath = request.at("old_path").get<std::string>();
  const std::string& newPath = request.at("new_path").get<std::string>();
  vtkVLogF(VTKSERVICESCORE_LOG_VERBOSITY(), "Rename(%s, %s)", oldPath.c_str(), newPath.c_str());
  vtkNJson reply;
  std::string oldFullPath;
  if (!this->IsPathAllowed(oldPath, oldFullPath))
  {
    reply["status"] = false;
    reply["__vtk_error_message__"] = PermissionErrorMessage(oldPath);
    return reply;
  }
  std::string newFullPath;
  if (!this->IsPathAllowed(newPath, newFullPath))
  {
    reply["status"] = false;
    reply["__vtk_error_message__"] = PermissionErrorMessage(newPath);
    return reply;
  }
  const bool status = (vtkDirectory::Rename(oldFullPath.c_str(), newFullPath.c_str()) == 1);
  reply["status"] = status;
  if (!status)
  {
    reply["__vtk_error_message__"] = std::strerror(errno);
  }
  return reply;
}

//-----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::ListDirectory(const vtkNJson& request) const
{
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "%s", __func__);
  vtkNJson reply;
  vtkNew<vtkPVFileInformation> info;
  // initialize the information object.
  info->LoadState(request.at("state"));
  // for(const auto& item : this->Mapped2RealPath)
  //{
  //  vtkLogF(INFO,"key %s, path %s",item.first.c_str(),item.second.c_str());
  //}

  if (!this->IsMappedRoot(info->GetPath(), info))
  {
    std::string resolvedPath;
    if (!this->IsPathAllowed(info->GetPath(), resolvedPath))
    {
      info->Initialize();
      reply["status"] = false;
      reply["state"] = info->SaveInformation();
      reply["__vtk_error_message__"] = PermissionErrorMessage(resolvedPath);
      return reply;
    }
    info->SetPath(resolvedPath);

    info->GatherInformation(nullptr);
    this->TransformPaths(info);
  }
  reply["status"] = true;
  reply["state"] = info->SaveInformation();
  return reply;
}

//----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::FileExists(const vtkNJson& request) const
{
  const std::string& path = request.at("path").get<std::string>();
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "%s(%s)", __func__, path.c_str());
  vtkNJson reply;
  std::string fullPath;
  if (!this->IsPathAllowed(path, fullPath))
  {
    reply["status"] = false;
    reply["__vtk_error_message__"] = PermissionErrorMessage(fullPath);
    return reply;
  }
  // also checks if a directory named `fullPath` exists in the remote file system.
  const bool status = vtksys::SystemTools::FileExists(fullPath);
  reply["status"] = status;
  reply["path"] = fullPath;
  return reply;
}

//----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::AddRootDirectory(const vtkNJson& request)
{
  const std::string& mappedPath = request.at("mapped_path");
  const std::string& realPath = request.at("real_path");
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "%s(%s, %s)", __func__, mappedPath.c_str(),
    realPath.c_str());

  this->Mapped2RealPath[mappedPath] = realPath;

  const bool status = true;
  vtkNJson reply;
  reply["status"] = status;
  return reply;
}

//----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::RemoveRootDirectory(const vtkNJson& request)
{
  const std::string& mappedPath = request.at("mapped_path");
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "%s(%s)", __func__, mappedPath.c_str());
  vtkNJson reply;

  size_t numberOfErasedElements = this->Mapped2RealPath.erase(mappedPath);
  if (numberOfErasedElements == 0)
  {
    reply["status"] = false;
    reply["__vtk_error_message__"] = mappedPath + " is not part of root directories";
  }
  else
  {
    reply["status"] = true;
  }

  return reply;
}
//----------------------------------------------------------------------------
vtkNJson vtkRemoteFileSystemProvider::vtkInternals::ConvertMappedPaths(
  const vtkNJson& request) const
{
  const std::vector<std::string>& mappedPaths = request.at("mapped_paths");
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "%s()", __func__);
  vtkNJson reply;

  vtkNJson realPaths = vtkNJson::array();
  for (const std::string& path : mappedPaths)
  {
    std::string realPath;
    if (!this->IsPathAllowed(path, realPath))
    {
      reply["status"] = false;
      reply["__vtk_error_message__"] = path + " is not part of root directories";
      return reply;
    }
    realPaths.emplace_back(realPath);
  }

  reply["status"] = true;
  reply["paths"] = realPaths;

  return reply;
}

//----------------------------------------------------------------------------
bool vtkRemoteFileSystemProvider::ParseResponse(
  const vtkPacket& packet, vtkPVFileInformation* information)
{
  return ParseResponse(packet.GetJSON(), information);
}
//----------------------------------------------------------------------------
bool vtkRemoteFileSystemProvider::ParseResponse(
  const vtkNJson& packet, vtkPVFileInformation* information)
{
  return information->LoadInformation(packet.at("state"));
}
//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::vtkInternals::Process(const vtkPacket& packet)
{
  const auto& request = packet.GetJSON();
  vtkVLogScopeF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(), "Process: %s", request.dump(-1).c_str());
  try
  {
    const auto& type = request.at("type").get<std::string>();

    if (type == "vtk-remote-filesystem-make-directory")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::MakeDirectory);
    }

    if (type == "vtk-remote-filesystem-remove-path")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::Remove);
    }

    if (type == "vtk-remote-filesystem-rename-path")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::Rename);
    }

    if (type == "vtk-remote-filesystem-list-directory")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::ListDirectory);
    }

    if (type == "vtk-remote-filesystem-file-exists")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::FileExists);
    }

    if (type == "vtk-remote-filesystem-add-root-directory")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::AddRootDirectory);
    }

    if (type == "vtk-remote-filesystem-remove-root-directory")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::RemoveRootDirectory);
    }

    if (type == "vtk-remote-filesystem-convert-mapped-paths")
    {
      return Call(request, &vtkRemoteFileSystemProvider::vtkInternals::ConvertMappedPaths);
    }

    const std::string errorMsg = "unknown rpc type" + type;
    return { { "__vtk_error_message__", errorMsg } };
  }
  catch (std::exception& exception)
  {
    const std::string errorMsg = exception.what();
    vtkLogF(ERROR, "ERROR processing FileProvider request: %s", errorMsg.c_str());
    return { { "__vtk_error_message__", errorMsg } };
  }
  return { { "__vtk_error_message__", "Uncaught Error" } };
}

//============================================================================
vtkObjectFactoryNewMacro(vtkRemoteFileSystemProvider);
//----------------------------------------------------------------------------
vtkRemoteFileSystemProvider::vtkRemoteFileSystemProvider()
  : Internals(new vtkRemoteFileSystemProvider::vtkInternals(this))
{
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(),
    "vtkRemoteFileSystemProvider::vtkRemoteFileSystemProvider()");
}

//----------------------------------------------------------------------------
vtkRemoteFileSystemProvider::~vtkRemoteFileSystemProvider()
{
  vtkVLogF(VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY(),
    "vtkRemoteFileSystemProvider::~vtkRemoteFileSystemProvider()");
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::MakeDirectory(
  const std::string& fullpath, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-make-directory";
  message["path"] = fullpath;
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::Remove(const std::string& fullpath, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-remove-path";
  message["path"] = fullpath;
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::Rename(
  const std::string& oldFullPath, const std::string& newFullPath, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-rename-path";
  message["old_path"] = oldFullPath;
  message["new_path"] = newFullPath;
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::ListDirectory(
  vtkPVFileInformation* information, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-list-directory";
  message["state"] = information->SaveState();
  message["run_on_rpc"] = information->CanRunOnRPC();
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::FileExists(
  const std::string& fullPath, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-file-exists";
  message["path"] = fullPath;
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::AddRootDirectory(
  const std::string& mappedPath, const std::string& realPath, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-add-root-directory";
  message["mapped_path"] = mappedPath;
  message["real_path"] = realPath;
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::RemoveRootDirectory(
  const std::string& mappedPath, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-remove-root-directory";
  message["mapped_path"] = mappedPath;
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::ConvertMappedPaths(
  const std::vector<std::string>& mappedPaths, bool clientSymmetricMPI)
{
  vtkNJson message;
  message["type"] = "vtk-remote-filesystem-convert-mapped-paths";
  message["mapped_paths"] = mappedPaths;
  message["run_on_root_only"] = true;
  message["client_uses_symmetric_mpi"] = clientSymmetricMPI;
  return { message };
}

//----------------------------------------------------------------------------
void vtkRemoteFileSystemProvider::InitializeInternal(vtkService* service)
{
  assert(service != nullptr);
  auto& internals = (*this->Internals);

  auto observable = service->GetRequestObservable(/*skipEventLoop*/ true);

  // since we want to handle each message in order it is received we create a
  // single processing stream.
  internals.Subscription =
    observable
      .filter([](const vtkServiceReceiver& receiver) {
        auto& json = receiver.GetPacket().GetJSON();
        auto iter = json.find("type");
        return iter != json.end() &&
          iter.value().get<std::string>().find("vtk-remote-filesystem-") != std::string::npos;
      })
      .map([this](const vtkServiceReceiver& receiver) {
        this->Preview(receiver.GetPacket());
        return receiver;
      })
      .filter([this](const vtkServiceReceiver& receiver) {
        // handle "run_on_rpc" requests.
        const auto& json = receiver.GetPacket().GetJSON();
        if (json.value("run_on_rpc", false))
        {
          receiver.Respond(this->ProcessOnRPC(receiver.GetPacket()));
          return false;
        }
        return true;
      })
      .filter([this](const vtkServiceReceiver& receiver) {
        // handle "run_on_root_only" requests.
        const auto& json = receiver.GetPacket().GetJSON();
        const bool run_on_root_only = json.value("run_on_root_only", false);
        const bool mpi_symmetric_mode = json.value("client_uses_symmetric_mpi", false);
        if (run_on_root_only && !mpi_symmetric_mode &&
          this->GetController()->GetLocalProcessId() != 0)
        {
          return false;
        }
        return true;
      })
      .observe_on(service->GetRunLoopScheduler())
      .subscribe([this](const vtkServiceReceiver& receiver) {
        receiver.Respond(this->Process(receiver.GetPacket()));
      });
}

//----------------------------------------------------------------------------
void vtkRemoteFileSystemProvider::Preview(const vtkPacket& packet)
{
  auto& internals = (*this->Internals);
  internals.Preview(packet);
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::ProcessOnRPC(const vtkPacket& packet)
{
  auto& internals = (*this->Internals);
  return internals.Process(packet);
}

//----------------------------------------------------------------------------
vtkPacket vtkRemoteFileSystemProvider::Process(const vtkPacket& packet)
{
  auto& internals = (*this->Internals);
  return internals.Process(packet);
}

//----------------------------------------------------------------------------
void vtkRemoteFileSystemProvider::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
