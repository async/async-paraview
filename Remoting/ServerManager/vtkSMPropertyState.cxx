/*=========================================================================

  Program:   ParaView
  Module:    vtkSMPropertyState.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkSMPropertyState.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkSMPropertyTypes.h"

#include <map>
#include <vector>

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkSMInputPropertyStateValue);

//-----------------------------------------------------------------------------
vtkSMInputPropertyStateValue::vtkSMInputPropertyStateValue() = default;

//-----------------------------------------------------------------------------
vtkSMInputPropertyStateValue::~vtkSMInputPropertyStateValue() = default;

//-----------------------------------------------------------------------------
void vtkSMInputPropertyStateValue::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Producer: " << this->Producer << '\n'
     << indent << "PortIndex: " << this->PortIndex << '\n';
}

//-----------------------------------------------------------------------------
vtkSMPropertyState::vtkSMPropertyState()
  : JSON(std::make_shared<vtkNJson>())
{
}

//-----------------------------------------------------------------------------
vtkSMPropertyState::vtkSMPropertyState(const vtkNJson& json)
  : JSON(std::make_shared<vtkNJson>(json))
{
}

//-----------------------------------------------------------------------------
vtkSMPropertyState::vtkSMPropertyState(vtkNJson&& json)
  : JSON(std::make_shared<vtkNJson>(std::move(json)))
{
}

//-----------------------------------------------------------------------------
vtkSMPropertyState::~vtkSMPropertyState() = default;

//-----------------------------------------------------------------------------
const vtkNJson& vtkSMPropertyState::GetJSON() const
{
  return *this->JSON;
}

//-----------------------------------------------------------------------------
void vtkSMPropertyState::Print(std::ostream& out) const
{
  out << this->JSON->dump(1) << std::endl;
}

//-----------------------------------------------------------------------------
std::string vtkSMPropertyState::DumpJSON() const
{
  return this->JSON->dump();
}

//-----------------------------------------------------------------------------
std::string vtkSMPropertyState::GetName() const
{
  return this->JSON->at("name");
}

//-----------------------------------------------------------------------------
void vtkSMPropertyState::SetName(const std::string& value)
{
  (*this->JSON)["name"] = value;
}

//-----------------------------------------------------------------------------
std::size_t vtkSMPropertyState::GetNumberOfValues() const
{
  return this->JSON->at("elements").size();
}

//-----------------------------------------------------------------------------
vtkVariant vtkSMPropertyState::GetValue(std::size_t index) const
{
  auto type = static_cast<vtkSMPropertyTypes::PropertyTypes>(this->JSON->at("type").get<int>());
  auto& value = this->JSON->at("elements")[index];
  switch (type)
  {
    case vtkSMPropertyTypes::INT:
      return vtkVariant(value.get<int>());
    case vtkSMPropertyTypes::DOUBLE:
      return vtkVariant(value.get<double>());
    case vtkSMPropertyTypes::ID_TYPE:
      return vtkVariant(value.get<vtkIdType>());
    case vtkSMPropertyTypes::STRING:
      return vtkVariant(value.get<std::string>());
    case vtkSMPropertyTypes::PROXY:
      return vtkVariant(value.get<vtkTypeUInt32>());
    case vtkSMPropertyTypes::INPUT:
    {
      vtkNew<vtkSMInputPropertyStateValue> inputProperty;
      inputProperty->SetPortIndex(value.at("index").get<unsigned int>());
      inputProperty->SetProducer(value.at("producer").get<vtkTypeUInt32>());
      return vtkVariant(inputProperty);
    }
    default:
      return {};
  }
}

//-----------------------------------------------------------------------------
void vtkSMPropertyState::SetValue(std::size_t index, const vtkVariant& value)
{
  auto type = static_cast<vtkSMPropertyTypes::PropertyTypes>(this->JSON->at("type").get<int>());
  auto& dst = this->JSON->at("elements")[index];
  switch (type)
  {
    case vtkSMPropertyTypes::INT:
      dst = value.ToInt();
    case vtkSMPropertyTypes::DOUBLE:
      dst = value.ToDouble();
    case vtkSMPropertyTypes::ID_TYPE:
      if (VTK_ID_TYPE_IMPL == VTK_LONG_LONG)
      {
        dst = value.ToLongLong();
      }
      else if (VTK_ID_TYPE_IMPL == VTK_LONG)
      {
        dst = value.ToLong();
      }
      else if (VTK_ID_TYPE_IMPL == VTK_INT)
      {
        dst = value.ToInt();
      }
    case vtkSMPropertyTypes::STRING:
      dst = value.ToString();
    case vtkSMPropertyTypes::PROXY:
      if (VTK_TYPE_UINT32 == VTK_UNSIGNED_INT)
      {
        dst = value.ToUnsignedInt();
      }
      else if (VTK_TYPE_UINT32 == VTK_UNSIGNED_LONG)
      {
        dst = value.ToUnsignedLong();
      }
    case vtkSMPropertyTypes::INPUT:
    {
      auto inputProperty = vtkSMInputPropertyStateValue::SafeDownCast(value.ToVTKObject());
      dst["index"] = inputProperty->GetPortIndex();
      dst["producer"] = inputProperty->GetProducer();
    }
    default:
      return;
  }
}
