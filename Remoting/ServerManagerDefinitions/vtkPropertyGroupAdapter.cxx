/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPropertyGroupAdapter.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPropertyGroupAdapter.h"
#include "vtkAdapterUtilities.h"
#include "vtkObjectFactory.h"
#include "vtkSMDocumentation.h"
#include "vtkSMPropertyGroup.h"
#include "vtkSMVectorProperty.h"
#include "vtkSmartPointer.h"

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPropertyGroupAdapter);

//-----------------------------------------------------------------------------
vtkPropertyGroupAdapter::vtkPropertyGroupAdapter() = default;

//-----------------------------------------------------------------------------
vtkPropertyGroupAdapter::~vtkPropertyGroupAdapter() = default;

//-----------------------------------------------------------------------------
void vtkPropertyGroupAdapter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "SMPropertyGroup " << this->SMPropertyGroup;
  this->SMPropertyGroup->PrintSelf(os, indent.GetNextIndent());
}

//-----------------------------------------------------------------------------
void vtkPropertyGroupAdapter::SetSMPropertyGroup(vtkSMPropertyGroup* group)
{
  this->SMPropertyGroup = group;
}

//------------------------------------------------------------------------------
std::string vtkPropertyGroupAdapter::GetName() const
{
  if (const char* name = this->SMPropertyGroup->GetName())
  {
    return name;
  }
  return this->Name;
}

//------------------------------------------------------------------------------
std::string vtkPropertyGroupAdapter::GetXMLLabel() const
{
  if (const char* name = this->SMPropertyGroup->GetXMLLabel())
  {
    return name;
  }
  return this->Name;
}

//------------------------------------------------------------------------------
std::string vtkPropertyGroupAdapter::GetPanelWidget() const
{
  if (const char* name = this->SMPropertyGroup->GetPanelWidget())
  {
    return name;
  }
  return "";
}

//------------------------------------------------------------------------------
std::string vtkPropertyGroupAdapter::GetPanelVisibility() const
{
  if (const char* name = this->SMPropertyGroup->GetPanelVisibility())
  {
    return name;
  }
  return "";
}

//------------------------------------------------------------------------------
void vtkPropertyGroupAdapter::SetName(const std::string& name)
{
  this->Name = name;
}

//------------------------------------------------------------------------------
std::vector<std::string> vtkPropertyGroupAdapter::GetWidgetDecorators() const
{
  return vtkAdapterUtilities::GetDecoratorNames(this->SMPropertyGroup->GetHints());
}
