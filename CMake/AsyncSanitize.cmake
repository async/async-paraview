# This code has been adapted from smtk (https://gitlab.kitware.com/cmb/smtk)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR
    CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
  set(CMAKE_COMPILER_IS_CLANGXX 1)
endif ()

if (CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_CLANGXX)
  # Add option for enabling sanitizers
  option(APV_ENABLE_SANITIZER "Build with sanitizer support." OFF)
  mark_as_advanced(APV_ENABLE_SANITIZER)

  if (APV_ENABLE_SANITIZER)
     # Pick among different sanitizers
    set(APV_SANITIZER "address"
      CACHE STRING "The sanitizer to use")
    mark_as_advanced(APV_SANITIZER)
    set_property(CACHE APV_SANITIZER PROPERTY STRINGS "address" "undefined" "thread")

    if (UNIX AND NOT APPLE)
        # Tests using external binaries need additional help to load the ASan
        # runtime when in use.
        find_library(APV_ASAN_LIBRARY
          NAMES asan libasan.so.6 libasan.so.5
          DOC "ASan library")
        mark_as_advanced(APV_ASAN_LIBRARY)

    endif ()

    set(async_sanitize_args
      "-fsanitize=${APV_SANITIZER}")

    target_compile_options(asyncbuild
      INTERFACE
        "$<BUILD_INTERFACE:${async_sanitize_args}>")
    target_link_options(asyncbuild
      INTERFACE
        "$<BUILD_INTERFACE:${async_sanitize_args}>")
  endif ()
endif ()
