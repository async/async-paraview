import asyncio

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager

from vtk.util.misc import vtkGetDataRoot
import os.path


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pm = PropertyManager()

    dataToLoad = os.path.join(vtkGetDataRoot(), "Testing/Data/rock.vti")

    reader = await builder.CreateProxy(
        "sources", "XMLImageDataReader", FileName=dataToLoad
    )

    status = await pm.UpdatePipeline(reader)
    assert status

    if App.num_ranks() > 1:
        info = await pm.GatherInformation(reader)
    else:
        info = reader.GetDataInformation()

    if App.rank() == 0:
        if App.num_ranks() == 1:
            assert info.GetNumberOfPoints() == 17139400
        assert info.GetNumberOfCells() == 16917660

    await App.close(session)


asyncio.run(main())
