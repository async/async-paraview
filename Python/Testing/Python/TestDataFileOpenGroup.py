import asyncio

from async_paraview.services import ParaT, DataFile, PropertyManager
from vtkmodules.vtkCommonCore import VTK_UNSTRUCTURED_GRID

from vtk.util.misc import vtkGetDataRoot
import os.path

expectedReaders = [("sources", "IOSSReader")]


async def main():
    App = ParaT()
    session = await App.initialize()

    datafileService = DataFile(session)
    pm = PropertyManager()

    filenames = [
        os.path.join(vtkGetDataRoot(), f"Testing/Data/can.e.4/can.e.4.{i}")
        for i in range(4)
    ]
    possibleReaders = await datafileService.FindPossibleReaders(filenames)
    assert possibleReaders == expectedReaders

    reader = await datafileService.Open(filenames)
    assert reader is not None

    success = await pm.UpdatePipeline(reader, 0)
    assert success == True
    if App.num_ranks() == 1:
        di = reader.GetDataInformation()
    else:
        di = reader.GetDataInformation()
        di = await pm.GatherInformation(reader)

    if App.rank() == 0:
        assert di.GetDataSetType() == VTK_UNSTRUCTURED_GRID
        assert di.GetNumberOfDataSets() == 5
        assert di.GetNumberOfPoints() == 10516
        assert di.GetNumberOfCells() == 7152

    await App.close(session)


asyncio.run(main())
