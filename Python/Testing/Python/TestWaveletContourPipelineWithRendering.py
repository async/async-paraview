import asyncio

from async_paraview.services import (
    ApplyController,
    ParaT,
    PipelineBuilder,
    PropertyManager,
)
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)
from async_paraview.testing import (
    apvtesting,
)
import vtk

async def anext_(observable):
    async for e in observable:
        break

async def main():
    App = ParaT()
    session = await App.initialize()
    builder = PipelineBuilder(session)
    acontroller = ApplyController(session)

    wavelet = await builder.CreateProxy("sources", "RTAnalyticSource")
    contour = await builder.CreateProxy("filters", "Contour", Input=wavelet)

    pmanager = PropertyManager()
    pmanager.SetValues(
        contour,
        ContourValues=[
            37.35310363769531,
            90.56993103027344,
            117.1783447265625,
            170.39517211914062,
            223.61199951171875,
            276.8288269042969,
        ],
        SelectInputScalars=["", "", "", "", "RTData"],
        force_push=True,
    )

    view = await builder.CreateProxy("views", "RenderView")
    # before we 'update' the view, let's install an interactor on the view
    # so that the viewport dimensions, cameras of the client and server's render windows sync up.
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(view.GetRenderWindow())

    # Apply will call UpdatePipeline() for proxies created since last time.
    # Create representations for them and apply any scalar coloring if available and update the view
    acontroller.Apply()
    await anext_(acontroller.GetObservable())
    regressionTestPassed = await apvtesting.DoRegressionTest(view)

    await App.close(session)
    assert(regressionTestPassed)


asyncio.run(main())
