import asyncio

from async_paraview.services import (
    anext,
    ApplyController,
    ParaT,
    PipelineBuilder,
    PropertyManager,
)
from async_paraview.testing import (
    apvtesting,
)

from vtkmodules.vtkRenderingCore import vtkRenderWindowInteractor


async def main():
    App = ParaT()
    session = await apvtesting.Connect(App)

    builder = PipelineBuilder(session)
    acontroller = ApplyController(session)
    pmanager = PropertyManager()

    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )
    shrink = await builder.CreateProxy(
        "filters", "ShrinkFilter", Input=sphere, ShrinkFactor=0.3
    )

    pmanager.SetValues(sphere, Radius=3, Center=[1, 2, 3], EndPhi=90)
    pmanager.Push(sphere, shrink)

    view = await builder.CreateProxy("views", "RenderView")
    values = pmanager.GetValues(view)
    # make sure ProxyProperties with nullptr as default value are transformed to None
    assert values["BackgroundTexture"] is None
    iren = vtkRenderWindowInteractor()
    iren.SetRenderWindow(view.GetRenderWindow())

    acontroller.Apply()
    await anext(acontroller.GetObservable())

    regressionTestPassed = await apvtesting.DoRegressionTest(view)

    status = await builder.DeleteProxy(shrink)
    assert status == True

    await App.close(session, exit_server=True)
    assert regressionTestPassed


asyncio.run(main())
