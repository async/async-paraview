import asyncio

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager
from vtkmodules.vtkRenderingCore import vtkRenderWindowInteractor
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)


async def main():
    app = ParaT()
    server_url = app.get_options().GetServerURL()
    if len(server_url) > 0:
        session = await app.initialize(url=server_url)
    else:
        session = await app.initialize()
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()

    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )
    shrink = await builder.CreateProxy(
        "filters", "ShrinkFilter", Input=sphere, ShrinkFactor=0.3
    )
    view = await builder.CreateProxy("views", "RenderView")
    iren = vtkRenderWindowInteractor()
    iren.SetRenderWindow(view.GetRenderWindow())

    representation = await builder.CreateRepresentation(
        shrink, 0, view, "GeometryRepresentation"
    )

    pmanager.SetVisibility(representation, view, True)
    await pmanager.Update(view)
    view.ResetCameraUsingVisiblePropBounds()

    save_screenshot = await builder.CreateProxy("misc", "SaveScreenshot")
    pmanager.SetValues(
        save_screenshot, force_push=True, **{"View": view, "Writer": "PNG"}
    )
    # get the writer and set filename to use
    writer = pmanager.GetValues(save_screenshot)["Writer"]
    values = {"FileName": "test_screenshot.png"}
    pmanager.SetValues(writer, **values, force_push=True)
    await pmanager.ExecuteCommand(save_screenshot, "Write")

    await app.close(session, exit_server=True)


asyncio.run(main())
