import asyncio

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pmanager = PropertyManager()

    ugrid = await builder.CreateProxy("sources", "FastUniformGrid")
    clip = await builder.CreateProxy("filters", "Clip", Input=ugrid)

    # print(pmanager.GetValues(clip))
    p = {
        "ClipFunction": None,  # None(nullptr in C++) corresponds to the NullProxy of the ProxyListDomain for this property
        "Value": 6,
        "SelectInputScalars": ["", "", "", "0", "DistanceSquared"],
    }

    pmanager.SetValues(clip, **p)
    pmanager.Push(clip)
    # print(pmanager.GetValues(clip))

    status = await pmanager.UpdatePipeline(clip)
    assert status
    if App.num_ranks() == 2:
        assert clip.GetDataInformation().GetNumberOfPoints() == 136
        assert clip.GetDataInformation().GetNumberOfCells() == 260
    elif App.num_ranks() == 1:
        assert clip.GetDataInformation().GetNumberOfPoints() == 231
        assert clip.GetDataInformation().GetNumberOfCells() == 520
    else:
        raise NotImplementedError("No baseline data for more than 2 ranks")

    await App.close(session)


asyncio.run(main())
