import asyncio
import sys

from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)
from async_paraview.modules.vtkRemotingServerManagerPython import (
    vtkPVPythonApplication,
)
from async_paraview.modules.vtkRemotingTestUtilities import (
    vtkAPVRegressionTester,
)
from vtkmodules.vtkTestingRendering import vtkTesting


async def DoRegressionTest(view, threshold=0.15):
    captured = vtkAPVRegressionTester.RenderAndCaptureView(view)
    await vtkPythonObservableWrapperUtilities.GetFuture(captured)
    status = vtkAPVRegressionTester.Test(
        sys.argv, view, vtkPVPythonApplication.GetInstance(), threshold
    )
    # Handle interactive mode.
    if status == vtkTesting.DO_INTERACTOR:
        iren = view.GetRenderWindowInteractor()
        # Unlike C++, pure python interaction will not be smooth on all platforms. trame integration is needed.
        while not iren.GetDone():
            iren.ProcessEvents()
            await asyncio.sleep(0.0001)
    return status != vtkTesting.FAILED


async def Connect(app):
    """
    Simple helper that connects to remote session if url is provided in the
    command line or to build-in session otherwise.
    """
    server_url = app.get_options().GetServerURL()
    if len(server_url) > 0:
        session = await app.initialize(url=server_url)
    else:
        session = await app.initialize()
    return session
