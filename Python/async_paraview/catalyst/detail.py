r"""
Internal functions not intended for use by users. This may change without
notice.
"""
# from .. import logger

from async_paraview.modules.vtkAPVInSitu import vtkInSituPipelinePython

from async_paraview.modules.vtkAPVPythonCatalyst import vtkCPPythonScriptV2Helper
from async_paraview.services import PropertyManager


def _get_active_helper():
    return vtkCPPythonScriptV2Helper.GetActiveInstance()


def _transform_registration_name(name):
    from . import get_args
    import re, argparse

    regex = re.compile(r"^\${args\.([\w.-]+)}$")
    m = regex.match(name)
    if not m:
        return name

    argname = m.group(1)
    parser = argparse.ArgumentParser()
    parser.add_argument("--%s" % argname, dest=argname)
    parser.add_argument("-%s" % argname, dest=argname)
    result = parser.parse_known_args(get_args())[0]
    val = getattr(result, argname)
    return val if val else name


def IsInsitu():
    """Returns True if executing in an insitu environment, else false"""
    helper = _get_active_helper()
    return helper is not None


if IsInsitu():
    from async_paraview.modules.vtkAPVInSitu import vtkInSituInitializationHelper


def IsCatalystInSituAPI():
    """Returns True if the active execution environment is from within
    an implementation of the Conduit-based Catalyst In Situ API."""
    return IsInsitu() and vtkInSituInitializationHelper.IsInitialized()


def IsInsituInput(name):
    if not name or not IsInsitu():
        return False
    name = _transform_registration_name(name)
    if IsCatalystInSituAPI() and (
        vtkInSituInitializationHelper.GetProducer(name) is not None
    ):
        # Catalyst 2.0
        return True
    return False


def RegisterExtractor(extractor):
    raise NotImplementedError
    """Keeps track of extractors created inside a specific Catalyst
    script.  This is useful to ensure we only update the extractors for that
    current script when multiple scripts are being executed in the same run.
    """
    assert IsInsitu()
    _get_active_helper().RegisterExtractor(extractor.SMProxy)


def RegisterView(view):
    """Keeps track of views created inside a specific Catalyst
    script.  This is useful to ensure we only update the views for the
    current script when multiple scripts are being executed in the same run.
    """
    assert IsInsitu()
    _get_active_helper().RegisterView(view)
    pmanager = PropertyManager()
    pmanager.SetValues(
        view, ViewTime=vtkInSituInitializationHelper.GetTime(), force_push=True
    )


def CreateProducer(name):
    assert IsInsituInput(name)
    # from . import log_level
    # from .. import log

    originalname = name
    name = _transform_registration_name(name)
    # log(
    #    log_level(),
    #    "creating producer for simulation input named '%s' (original-name=%s)"
    #    % (name, originalname),
    # )
    return vtkInSituInitializationHelper.GetProducer("grid")


def RegisterPackageFromZip(zipfilename, packagename=None):
    from . import importers

    zipfilename = _mpi_exchange_if_needed(zipfilename)
    return importers.add_file(zipfilename, packagename)


def RegisterPackageFromDir(path):
    import os.path
    from . import importers

    packagename = os.path.basename(path)
    init_py = os.path.join(path, "__init__.py")
    return importers.add_file(init_py, packagename)


def RegisterModuleFromFile(filename):
    from . import importers

    return importers.add_file(filename)


_temp_directory = None


def _mpi_exchange_if_needed(filename):
    raise NotImplementedError


#    global _temp_directory
#
#    from ..modules.vtkRemotingCore import vtkProcessModule
#    pm = vtkProcessModule.GetProcessModule()
#    if pm.GetNumberOfLocalPartitions() <= 1:
#        return filename
#
#    from mpi4py import MPI
#    from vtkmodules.vtkParallelMPI4Py import vtkMPI4PyCommunicator
#    comm = vtkMPI4PyCommunicator.ConvertToPython(pm.GetGlobalController().GetCommunicator())
#    if comm.Get_rank() == 0:
#        with open(filename, 'rb') as f:
#            data = f.read()
#    else:
#        data = None
#    data = comm.bcast(data, root=0)
#    if comm.Get_rank() == 0:
#        return filename
#    else:
#        if not _temp_directory:
#            import tempfile, os.path
#            # we hook the temp-dir to the module so it lasts for the livetime of
#            # the interpreter and gets cleaned up on exit.
#            _temp_directory = tempfile.TemporaryDirectory()
#        filename = os.path.join(_temp_directory.name, os.path.basename(filename))
#        with open(filename, "wb") as f:
#            f.write(data)
#        return filename
